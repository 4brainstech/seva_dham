﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class demo_pat_reg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.btn_register = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.DateTimePicker_dob = New System.Windows.Forms.DateTimePicker()
        Me.RichTextBox_address = New System.Windows.Forms.RichTextBox()
        Me.DateTimePicker_valid_from = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker_valid_till = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.radio_btn_male = New System.Windows.Forms.RadioButton()
        Me.Radio_btn_female = New System.Windows.Forms.RadioButton()
        Me.label_visit_counter = New System.Windows.Forms.Label()
        Me.TextBox_mob_no = New System.Windows.Forms.TextBox()
        Me.ComboBox_blood_grp = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(898, 552)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(219, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(0, 0, 180, 0)
        Me.Label1.Size = New System.Drawing.Size(639, 80)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "SEVADHAM HOSPITAL"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.PictureBox1)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(87, 83)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(129, 426)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Seva_Dham.My.Resources.Resources.img2
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(129, 383)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.AutoScroll = True
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.btn_register, 0, 9)
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label9, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label10, 0, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.txtname, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DateTimePicker_dob, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.RichTextBox_address, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.DateTimePicker_valid_from, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.DateTimePicker_valid_till, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.label_visit_counter, 1, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.TextBox_mob_no, 1, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.ComboBox_blood_grp, 1, 8)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(219, 80)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 10
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(639, 432)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'btn_register
        '
        Me.btn_register.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn_register.Dock = System.Windows.Forms.DockStyle.Right
        Me.btn_register.Font = New System.Drawing.Font("Times New Roman", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_register.Location = New System.Drawing.Point(163, 543)
        Me.btn_register.Name = "btn_register"
        Me.btn_register.Size = New System.Drawing.Size(153, 34)
        Me.btn_register.TabIndex = 22
        Me.btn_register.Text = "REGISTER"
        Me.btn_register.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 180)
        Me.Label8.Name = "Label8"
        Me.Label8.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label8.Size = New System.Drawing.Size(286, 24)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Valid From"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 120)
        Me.Label6.Name = "Label6"
        Me.Label6.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label6.Size = New System.Drawing.Size(259, 24)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label4.Size = New System.Drawing.Size(226, 24)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "DOB"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 240)
        Me.Label3.Name = "Label3"
        Me.Label3.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label3.Size = New System.Drawing.Size(269, 24)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Valid Till"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 300)
        Me.Label9.Name = "Label9"
        Me.Label9.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label9.Size = New System.Drawing.Size(255, 24)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Gender"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 360)
        Me.Label5.Name = "Label5"
        Me.Label5.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label5.Size = New System.Drawing.Size(308, 24)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Visit Counter"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 420)
        Me.Label7.Name = "Label7"
        Me.Label7.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label7.Size = New System.Drawing.Size(286, 24)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Mobile No."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label2.Size = New System.Drawing.Size(240, 24)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Name"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 480)
        Me.Label10.Name = "Label10"
        Me.Label10.Padding = New System.Windows.Forms.Padding(180, 0, 0, 0)
        Me.Label10.Size = New System.Drawing.Size(297, 24)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Blood group"
        '
        'txtname
        '
        Me.txtname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtname.Location = New System.Drawing.Point(322, 3)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(200, 22)
        Me.txtname.TabIndex = 9
        '
        'DateTimePicker_dob
        '
        Me.DateTimePicker_dob.Location = New System.Drawing.Point(322, 63)
        Me.DateTimePicker_dob.Name = "DateTimePicker_dob"
        Me.DateTimePicker_dob.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker_dob.TabIndex = 10
        '
        'RichTextBox_address
        '
        Me.RichTextBox_address.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox_address.Location = New System.Drawing.Point(322, 123)
        Me.RichTextBox_address.Name = "RichTextBox_address"
        Me.RichTextBox_address.Size = New System.Drawing.Size(200, 54)
        Me.RichTextBox_address.TabIndex = 11
        Me.RichTextBox_address.Text = ""
        '
        'DateTimePicker_valid_from
        '
        Me.DateTimePicker_valid_from.Location = New System.Drawing.Point(322, 183)
        Me.DateTimePicker_valid_from.Name = "DateTimePicker_valid_from"
        Me.DateTimePicker_valid_from.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker_valid_from.TabIndex = 12
        '
        'DateTimePicker_valid_till
        '
        Me.DateTimePicker_valid_till.Location = New System.Drawing.Point(322, 243)
        Me.DateTimePicker_valid_till.Name = "DateTimePicker_valid_till"
        Me.DateTimePicker_valid_till.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker_valid_till.TabIndex = 13
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.radio_btn_male)
        Me.FlowLayoutPanel1.Controls.Add(Me.Radio_btn_female)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(319, 300)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(320, 60)
        Me.FlowLayoutPanel1.TabIndex = 14
        '
        'radio_btn_male
        '
        Me.radio_btn_male.AutoSize = True
        Me.radio_btn_male.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_btn_male.Location = New System.Drawing.Point(3, 3)
        Me.radio_btn_male.Name = "radio_btn_male"
        Me.radio_btn_male.Size = New System.Drawing.Size(60, 20)
        Me.radio_btn_male.TabIndex = 0
        Me.radio_btn_male.TabStop = True
        Me.radio_btn_male.Text = "Male"
        Me.radio_btn_male.UseVisualStyleBackColor = True
        '
        'Radio_btn_female
        '
        Me.Radio_btn_female.AutoSize = True
        Me.Radio_btn_female.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio_btn_female.Location = New System.Drawing.Point(69, 3)
        Me.Radio_btn_female.Name = "Radio_btn_female"
        Me.Radio_btn_female.Size = New System.Drawing.Size(78, 20)
        Me.Radio_btn_female.TabIndex = 1
        Me.Radio_btn_female.TabStop = True
        Me.Radio_btn_female.Text = "Female"
        Me.Radio_btn_female.UseVisualStyleBackColor = True
        '
        'label_visit_counter
        '
        Me.label_visit_counter.AutoSize = True
        Me.label_visit_counter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_visit_counter.Location = New System.Drawing.Point(322, 360)
        Me.label_visit_counter.Name = "label_visit_counter"
        Me.label_visit_counter.Size = New System.Drawing.Size(78, 16)
        Me.label_visit_counter.TabIndex = 15
        Me.label_visit_counter.Text = "Total Visit"
        '
        'TextBox_mob_no
        '
        Me.TextBox_mob_no.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox_mob_no.Location = New System.Drawing.Point(322, 423)
        Me.TextBox_mob_no.Name = "TextBox_mob_no"
        Me.TextBox_mob_no.Size = New System.Drawing.Size(200, 22)
        Me.TextBox_mob_no.TabIndex = 16
        '
        'ComboBox_blood_grp
        '
        Me.ComboBox_blood_grp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox_blood_grp.FormattingEnabled = True
        Me.ComboBox_blood_grp.Items.AddRange(New Object() {"A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"})
        Me.ComboBox_blood_grp.Location = New System.Drawing.Point(322, 483)
        Me.ComboBox_blood_grp.Name = "ComboBox_blood_grp"
        Me.ComboBox_blood_grp.Size = New System.Drawing.Size(200, 24)
        Me.ComboBox_blood_grp.TabIndex = 17
        '
        'demo_pat_reg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(898, 552)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "demo_pat_reg"
        Me.Text = "demo_pat_reg"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker_dob As System.Windows.Forms.DateTimePicker
    Friend WithEvents RichTextBox_address As System.Windows.Forms.RichTextBox
    Friend WithEvents DateTimePicker_valid_from As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker_valid_till As System.Windows.Forms.DateTimePicker
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents radio_btn_male As System.Windows.Forms.RadioButton
    Friend WithEvents Radio_btn_female As System.Windows.Forms.RadioButton
    Friend WithEvents label_visit_counter As System.Windows.Forms.Label
    Friend WithEvents TextBox_mob_no As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox_blood_grp As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btn_register As System.Windows.Forms.Button
End Class
