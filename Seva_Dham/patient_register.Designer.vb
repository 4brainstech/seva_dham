﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class patient_register
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.rtxt_address = New System.Windows.Forms.RichTextBox()
        Me.dl_blood = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dtp_dob = New System.Windows.Forms.DateTimePicker()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtp_v_from = New System.Windows.Forms.DateTimePicker()
        Me.dtp_v_till = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_mobile = New System.Windows.Forms.TextBox()
        Me.ddl_visit_counter = New System.Windows.Forms.ComboBox()
        Me.rbtn_male = New System.Windows.Forms.RadioButton()
        Me.rbtn_female = New System.Windows.Forms.RadioButton()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(535, 118)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(535, 168)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 22)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "DOB"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(539, 418)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 22)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Gender"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(539, 595)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 22)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Blood Group"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(525, 218)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 22)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Address"
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(710, 100)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(230, 26)
        Me.txt_name.TabIndex = 6
        '
        'rtxt_address
        '
        Me.rtxt_address.Location = New System.Drawing.Point(710, 202)
        Me.rtxt_address.Name = "rtxt_address"
        Me.rtxt_address.Size = New System.Drawing.Size(230, 65)
        Me.rtxt_address.TabIndex = 12
        Me.rtxt_address.Text = ""
        '
        'dl_blood
        '
        Me.dl_blood.FormattingEnabled = True
        Me.dl_blood.Items.AddRange(New Object() {"NA", "A+", "O+", "B+", "AB+", "A-", "O-", "B-", "AB-"})
        Me.dl_blood.Location = New System.Drawing.Point(710, 595)
        Me.dl_blood.Name = "dl_blood"
        Me.dl_blood.Size = New System.Drawing.Size(230, 27)
        Me.dl_blood.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Lucida Fax", 22.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label7.Location = New System.Drawing.Point(610, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(406, 36)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "PATIENT REGISTRATION"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.Seva_Dham.My.Resources.Resources.add_user
        Me.PictureBox1.Location = New System.Drawing.Point(482, 51)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Times New Roman", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(330, 492)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(153, 39)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "REGISTER"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dtp_dob
        '
        Me.dtp_dob.Location = New System.Drawing.Point(710, 151)
        Me.dtp_dob.Name = "dtp_dob"
        Me.dtp_dob.Size = New System.Drawing.Size(200, 26)
        Me.dtp_dob.TabIndex = 14
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(353, 396)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(55, 19)
        Me.lblMessage.TabIndex = 22
        Me.lblMessage.Text = "Label8"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(539, 308)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 19)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Valid From"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(539, 345)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 19)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Valid Till"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(541, 471)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 19)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Visit Counter"
        '
        'dtp_v_from
        '
        Me.dtp_v_from.Location = New System.Drawing.Point(710, 306)
        Me.dtp_v_from.Name = "dtp_v_from"
        Me.dtp_v_from.Size = New System.Drawing.Size(200, 26)
        Me.dtp_v_from.TabIndex = 27
        '
        'dtp_v_till
        '
        Me.dtp_v_till.Location = New System.Drawing.Point(710, 343)
        Me.dtp_v_till.Name = "dtp_v_till"
        Me.dtp_v_till.Size = New System.Drawing.Size(200, 26)
        Me.dtp_v_till.TabIndex = 28
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(553, 525)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 19)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Mobile_No"
        '
        'txt_mobile
        '
        Me.txt_mobile.Location = New System.Drawing.Point(710, 527)
        Me.txt_mobile.Name = "txt_mobile"
        Me.txt_mobile.Size = New System.Drawing.Size(126, 26)
        Me.txt_mobile.TabIndex = 30
        '
        'ddl_visit_counter
        '
        Me.ddl_visit_counter.FormattingEnabled = True
        Me.ddl_visit_counter.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"})
        Me.ddl_visit_counter.Location = New System.Drawing.Point(710, 471)
        Me.ddl_visit_counter.Name = "ddl_visit_counter"
        Me.ddl_visit_counter.Size = New System.Drawing.Size(121, 27)
        Me.ddl_visit_counter.TabIndex = 31
        '
        'rbtn_male
        '
        Me.rbtn_male.AutoSize = True
        Me.rbtn_male.Location = New System.Drawing.Point(710, 418)
        Me.rbtn_male.Name = "rbtn_male"
        Me.rbtn_male.Size = New System.Drawing.Size(63, 23)
        Me.rbtn_male.TabIndex = 32
        Me.rbtn_male.TabStop = True
        Me.rbtn_male.Text = "Male"
        Me.rbtn_male.UseVisualStyleBackColor = True
        '
        'rbtn_female
        '
        Me.rbtn_female.AutoSize = True
        Me.rbtn_female.Location = New System.Drawing.Point(836, 418)
        Me.rbtn_female.Name = "rbtn_female"
        Me.rbtn_female.Size = New System.Drawing.Size(76, 23)
        Me.rbtn_female.TabIndex = 33
        Me.rbtn_female.TabStop = True
        Me.rbtn_female.Text = "Female"
        Me.rbtn_female.UseVisualStyleBackColor = True
        '
        'patient_register
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1354, 733)
        Me.Controls.Add(Me.rbtn_female)
        Me.Controls.Add(Me.rbtn_male)
        Me.Controls.Add(Me.ddl_visit_counter)
        Me.Controls.Add(Me.txt_mobile)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dtp_v_till)
        Me.Controls.Add(Me.dtp_v_from)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.dl_blood)
        Me.Controls.Add(Me.dtp_dob)
        Me.Controls.Add(Me.rtxt_address)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "patient_register"
        Me.Text = "patient_register"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_name As System.Windows.Forms.TextBox
    Friend WithEvents rtxt_address As System.Windows.Forms.RichTextBox
    Friend WithEvents dl_blood As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dtp_dob As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtp_v_from As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_v_till As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_mobile As System.Windows.Forms.TextBox
    Friend WithEvents ddl_visit_counter As System.Windows.Forms.ComboBox
    Friend WithEvents rbtn_male As System.Windows.Forms.RadioButton
    Friend WithEvents rbtn_female As System.Windows.Forms.RadioButton
End Class
